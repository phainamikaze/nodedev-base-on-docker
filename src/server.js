'use strict';

const express = require('express');

// Constants
const PORT = process.env['PORT'] || 3000;
const HOST = '0.0.0.0';

console.log('NODE_ENV: '+process.env['NODE_ENV']);

// App
const app = express();
app.get('/', (req, res) => {
  res.send('hello world');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);