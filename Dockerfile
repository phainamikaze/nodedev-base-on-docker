ARG NODE_VERSION=10.16.0

#The instructions for the first stage
FROM node:${NODE_VERSION}-alpine as builder

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

#if require node-gyp and extra native installed os libraries to use.
#RUN apk --no-cache add python make g++

COPY package*.json ./
RUN npm install

# The instructions for second stage
FROM node:${NODE_VERSION}-alpine

ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

RUN if [ "$NODE_ENV" = "development" ] ; then echo installing nodemon && npm install -g nodemon ; else echo not install nodemon ; fi

WORKDIR /usr/src/app
COPY --from=builder node_modules node_modules

COPY . .

CMD [ "npm", "run", "start:prod" ]